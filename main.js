import { checkBoard } from "./checkBoard.js";

window.onload=function(){
    let gameBoard = [
        ['', '', ''],
        ['', '', ''],
        ['', '', '']
        ];

    let xElem= document.createElement("img");
    xElem.src="imgs/xxl-x.svg";

    let zeroElem= document.createElement("img");
    zeroElem.src="imgs/xxl-zero.svg";

    let inputClick1 =document.getElementById("cell1");
    let inputClick2 =document.getElementById("cell2");
    let inputClick3 =document.getElementById("cell3");
    let inputClick4 =document.getElementById("cell4");
    let inputClick5 =document.getElementById("cell5");
    let inputClick6 =document.getElementById("cell6");
    let inputClick7 =document.getElementById("cell7");
    let inputClick8 =document.getElementById("cell8");
    let inputClick9 =document.getElementById("cell9");

    let onPlayer = 1;

    let empty1 = 0;
    let empty2 = 0;
    let empty3 = 0;
    let empty4 = 0;
    let empty5 = 0;
    let empty6 = 0;
    let empty7 = 0;
    let empty8 = 0;
    let empty9 = 0;

    inputClick1.onclick= function(){
        switch (empty1){
            case 0:
                switch (onPlayer){
                case 1:
                    document.getElementById("cell1").appendChild(xElem);
                    onPlayer= 0;
                    empty1=1;
                    gameBoard[0][0] = "x";
                    break;
                case 0:
                    document.getElementById("cell1").appendChild(zeroElem);
                    onPlayer= 1;
                    empty1=1;
                    gameBoard[0][0] = "o";
                    break; 
                }
                break;
            case 1:
                break;
        }
        console.log(gameBoard);
        checkBoard(gameBoard);
    }

    inputClick2.onclick= function(){
        switch (empty2){
            case 0:
                switch (onPlayer){
                case 1:
                    document.getElementById("cell2").appendChild(xElem);
                    onPlayer= 0;
                    empty2=1;
                    gameBoard[0][1] = "x";
                    break;
                case 0:
                    document.getElementById("cell2").appendChild(zeroElem);
                    onPlayer= 1;
                    empty2=1;
                    gameBoard[0][1] = "o";
                    break; 
                }
                break;
            case 1:
                break;
        }
        console.log(gameBoard);
        checkBoard(gameBoard)
    }

    inputClick3.onclick= function(){
        switch (empty3){
            case 0:
                switch (onPlayer){
                case 1:
                    document.getElementById("cell3").appendChild(xElem);
                    onPlayer= 0;
                    empty3=1;
                    gameBoard[0][2] = "x";
                    break;
                case 0:
                    document.getElementById("cell3").appendChild(zeroElem);
                    onPlayer= 1;
                    empty3=1;
                    gameBoard[0][2] = "o";
                    break; 
                }
                break;
            case 1:
                break;
        }
        console.log(gameBoard);
        checkBoard(gameBoard)
    }

    inputClick4.onclick= function(){
        switch (empty4){
            case 0:
                switch (onPlayer){
                case 1:
                    document.getElementById("cell4").appendChild(xElem);
                    onPlayer= 0;
                    empty4=1;
                    gameBoard[1][0] = "x";
                    break;
                case 0:
                    document.getElementById("cell4").appendChild(zeroElem);
                    onPlayer= 1;
                    empty4=1;
                    gameBoard[1][0] = "o";
                    break; 
                }
                break;
            case 1:
                break;
        }
        console.log(gameBoard);
        checkBoard(gameBoard)
    }

    inputClick5.onclick= function(){
        switch (empty5){
            case 0:
                switch (onPlayer){
                case 1:
                    document.getElementById("cell5").appendChild(xElem);
                    onPlayer= 0;
                    empty5=1;
                    gameBoard[1][1] = "x";
                    break;
                case 0:
                    document.getElementById("cell5").appendChild(zeroElem);
                    onPlayer= 1;
                    empty5=1;
                    gameBoard[1][1] = "o";
                    break; 
                }
                break;
            case 1:
                break;
        }
        console.log(gameBoard);
        checkBoard(gameBoard)
    }

    inputClick6.onclick= function(){
        switch (empty6){
            case 0:
                switch (onPlayer){
                case 1:
                    document.getElementById("cell6").appendChild(xElem);
                    onPlayer= 0;
                    empty6=1;
                    gameBoard[1][2] = "x";
                    break;
                case 0:
                    document.getElementById("cell6").appendChild(zeroElem);
                    onPlayer= 1;
                    empty6=1;
                    gameBoard[1][2] = "o";
                    break; 
                }
                break;
            case 1:
                break;
        }
        console.log(gameBoard);
        checkBoard(gameBoard)
    }

    inputClick7.onclick= function(){
        switch (empty7){
            case 0:
                switch (onPlayer){
                case 1:
                    document.getElementById("cell7").appendChild(xElem);
                    onPlayer= 0;
                    empty7=1;
                    gameBoard[2][0] = "x";
                    break;
                case 0:
                    document.getElementById("cell7").appendChild(zeroElem);
                    onPlayer= 1;
                    empty6=1;
                    gameBoard[2][0] = "o";
                    break; 
                }
                break;
            case 1:
                break;
        }
        console.log(gameBoard);
        checkBoard(gameBoard)
    }

    inputClick8.onclick= function(){
        switch (empty8){
            case 0:
                switch (onPlayer){
                case 1:
                    document.getElementById("cell8").appendChild(xElem);
                    onPlayer= 0;
                    empty8=1;
                    gameBoard[2][1] = "x";
                    break;
                case 0:
                    document.getElementById("cell8").appendChild(zeroElem);
                    onPlayer= 1;
                    empty8=1;
                    gameBoard[2][1] = "o";
                    break; 
                }
                break;
            case 1:
                break;
        }
        console.log(gameBoard);
        checkBoard(gameBoard)
    }

    inputClick9.onclick= function(){
        switch (empty9){
            case 0:
                switch (onPlayer){
                case 1:
                    document.getElementById("cell9").appendChild(xElem);
                    onPlayer= 0;
                    empty9=1;
                    gameBoard[2][2] = "x";
                    break;
                case 0:
                    document.getElementById("cell9").appendChild(zeroElem);
                    onPlayer= 1;
                    empty9=1;
                    gameBoard[2][2] = "o";
                    break; 
                }
                break;
            case 1:
                break;
        }
        console.log(gameBoard);
        checkBoard(gameBoard)
    }
}
